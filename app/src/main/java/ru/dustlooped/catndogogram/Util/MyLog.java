package ru.dustlooped.catndogogram.Util;

/**
 * Created by dustlooped on 31.05.2016.
 */
public class MyLog {

    private static boolean isLoggingEnabled = false;
    private static final String  LOG_TAG = "CatnDogoGram";

    public static void setEnabled(boolean enabled) {
        isLoggingEnabled = enabled;
    }

    public static void log(String message) {
        if(isLoggingEnabled){
            android.util.Log.d(LOG_TAG, message);
        }
    }
}
