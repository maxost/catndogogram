package ru.dustlooped.catndogogram.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dustlooped on 01.06.2016.
 */
public class TimeConvert {
    public static String getDateStringFromLong (long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd.MM.yyyy");
        Date date = new Date();
        date.setTime(time*1000);
        return sdf.format(date);
    }
}