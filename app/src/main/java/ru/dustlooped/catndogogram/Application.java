package ru.dustlooped.catndogogram;

import ru.dustlooped.catndogogram.Cache.PreferencesCache;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 04.06.2016.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        log("Application onCreate");
        PreferencesCache.getInstance().initialize(this);
    }
}
