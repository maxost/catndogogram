package ru.dustlooped.catndogogram.Model;

import java.io.Serializable;

/**
 * Created by dustlooped on 31.05.2016.
 */
public class Post {

    private long id;
    private String username;
    private String imageURL;
    private String tags;
    private long   time;

    //for serialization
    public final static String FIELD_NAME_ID        = "id";
    public final static String FIELD_NAME_USERNAME  = "username";
    public final static String FIELD_NAME_IMAGE_URL = "imageURL";
    public final static String FIELD_NAME_TAGS      = "tags";
    public final static String FIELD_NAME_TIME      = "time";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null) return false;
        if (getClass()!=o.getClass()) return false;
        Post other = (Post) o;
        if(id==other.getId()) return true;
        else return false;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(id).hashCode();
    }
}