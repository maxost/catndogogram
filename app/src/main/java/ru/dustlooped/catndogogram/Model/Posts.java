package ru.dustlooped.catndogogram.Model;

import java.util.ArrayList;

/**
 * Created by dustlooped on 04.06.2016.
 */
public class Posts {
    int page;
    int pagesCount;
    ArrayList<Post> postsList = new ArrayList<>();

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public ArrayList<Post> getPostsList() {
        return postsList;
    }

    public void setPostsList(ArrayList<Post> postsList) {
        this.postsList = postsList;
    }
}
