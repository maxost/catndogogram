package ru.dustlooped.catndogogram.Cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.Model.Posts;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 04.06.2016.
 */
public class PreferencesCache implements Cache {

    private final static String CACHE_PREFERENCE_NAME = "posts_cache";
    private final static String CACHE_POSTS_KEY = "posts_cache_key";

    private static PreferencesCache instance = new PreferencesCache();
    private Context appContext;
    private SharedPreferences mPreferences;
    private ArrayList<Post> mPostsList = new ArrayList<>();

    public void initialize(Context context) {
        log("PreferencesCache initialize");
        appContext = context;
        mPreferences = appContext.getSharedPreferences(CACHE_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized PreferencesCache getInstance() {
        return instance;
    }

    @Override
    public void putPosts(Posts posts) {
        log("PreferencesCache putPosts");
        mPostsList.addAll(posts.getPostsList());

        try {
            mPreferences.edit().putString(CACHE_POSTS_KEY, getJsonFromPostsList(mPostsList)).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Post> getPostsList() {
        log("PreferencesCache getPostsList");
        String json = mPreferences.getString(CACHE_POSTS_KEY, null);
        mPostsList.clear();
        try {
            mPostsList.addAll(getPostsListFromJson(json));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        log("PreferencesCache getPostsList size = " + mPostsList.size());
        return mPostsList;
    }

    @Override
    public void clearCache() {
        log("PreferencesCache clearCache");
        mPostsList.clear();
        mPreferences.edit().putString(CACHE_POSTS_KEY, "");
    }

    private String getJsonFromPostsList(ArrayList<Post> postsList) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for(Post post : postsList) {
            JSONObject object = new JSONObject();
            object.putOpt(Post.FIELD_NAME_ID,        post.getId());
            object.putOpt(Post.FIELD_NAME_USERNAME,  post.getUsername());
            object.putOpt(Post.FIELD_NAME_IMAGE_URL, post.getImageURL());
            object.putOpt(Post.FIELD_NAME_TAGS,      post.getTags());
            object.putOpt(Post.FIELD_NAME_TIME,      post.getTime());
            jsonArray.put(object);
        }
        return jsonArray.toString();
    }

    private ArrayList<Post> getPostsListFromJson(String json) throws JSONException {
        ArrayList<Post> postList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(json);
        for(int i=0; i<jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Post post = new Post();

            post.setId        (object.getLong  (Post.FIELD_NAME_ID));
            post.setUsername  (object.getString(Post.FIELD_NAME_USERNAME));
            post.setTime      (object.getLong  (Post.FIELD_NAME_TIME));
            post.setImageURL  (object.getString(Post.FIELD_NAME_IMAGE_URL));
            post.setTags      (object.getString(Post.FIELD_NAME_TAGS));
            postList.add(post);
        }
        return postList;
    }
}