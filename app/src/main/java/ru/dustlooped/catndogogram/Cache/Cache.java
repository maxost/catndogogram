package ru.dustlooped.catndogogram.Cache;

import java.util.ArrayList;

import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.Model.Posts;

/**
 * Created by dustlooped on 04.06.2016.
 */
public interface Cache {
    void putPosts(Posts posts);
    ArrayList<Post> getPostsList();
    void clearCache();
}