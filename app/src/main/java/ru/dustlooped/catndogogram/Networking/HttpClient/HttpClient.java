package ru.dustlooped.catndogogram.Networking.HttpClient;

import java.io.IOException;
import java.net.URL;

/**
 * Created by dustlooped on 02.06.2016.
 */
public interface HttpClient {
    String GET(URL url) throws IOException;
}