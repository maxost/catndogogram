package ru.dustlooped.catndogogram.Networking.HttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 01.06.2016.
 */
public class BuiltInHttpClient implements HttpClient {

    @Override
    public String GET(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setConnectTimeout(5000);
        urlConnection.setReadTimeout(15000);
        urlConnection.connect();

        InputStream inputStream = urlConnection.getInputStream();
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        String resultJson = builder.toString();
        log("BuiltInHttpClient GET response = " + resultJson);
        return resultJson;
    }
}