package ru.dustlooped.catndogogram.Networking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.Model.Posts;

/**
 * Created by dustlooped on 30.05.2016.
 */
public interface Networking {
    Posts getPosts(HashSet<String> tags, int currentPage, boolean isANDmode) throws Exception;
}
