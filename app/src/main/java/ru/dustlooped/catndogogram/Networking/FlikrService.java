package ru.dustlooped.catndogogram.Networking;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;

import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.Model.Posts;
import ru.dustlooped.catndogogram.Networking.HttpClient.HttpClient;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 30.05.2016.
 */
public class FlikrService implements Networking {

    private final static String API_KEY = "a791306e5fd2eac22e96fb6409cabfe6";
    private final static int POSTS_COUNT_PER_PAGE = 10;

    private HttpClient mHttpClient;

    public FlikrService(HttpClient httpClient) {
        this.mHttpClient = httpClient;
    }

    @Override
    public Posts getPosts(final HashSet<String> tags, final int page, final boolean isANDmode)
            throws Exception {
        log("FlikrService getPostsList; tags = " + tags.toString()
                + "; page = " + page + "; isANDmode = " + isANDmode);
        URL url = buildURL(tags, page, isANDmode);
        String json = getJSON(url);
        if(json==null) throw new IOException();
        return parseJSON(json);
    }

    private String tagsToString(HashSet<String> tags) {
        StringBuilder builder = new StringBuilder();
        for(String tag : tags) {
            builder.append(tag).append(",");
        }
        return builder.toString();
    }

    private URL buildURL(HashSet<String> tags, int currentPage, boolean isANDmode)
            throws UnsupportedEncodingException, MalformedURLException {

        String resultTags = URLEncoder.encode(tagsToString(tags), "UTF-8"); //in case of cyrillic tag

        StringBuilder builder = new StringBuilder()
                .append("https://api.flickr.com/services/rest/?method=flickr.photos.search")
                .append("&extras=url_m%2C+owner_name%2Cdate_upload%2Ctags%2Cdescription")
                .append("&format=json&nojsoncallback=1")

                .append("&api_key=").append(API_KEY)
                .append("&tags=").append(resultTags)
                .append("&per_page=").append(POSTS_COUNT_PER_PAGE)
                .append("&page=").append(currentPage);
        if(isANDmode) builder.append("&tag_mode=all");

        return new URL(builder.toString());
    }

    @Nullable
    private String getJSON(URL url) {
        log("FlikrService getJSON");

        String json;
        try {
            json = mHttpClient.GET(url);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    private Posts parseJSON(String json) throws JSONException {
        log("FlikrService parseJSON");
        if(json==null) return null;

        JSONObject dataJsonObj;
        Posts posts = new Posts();
        dataJsonObj = new JSONObject(json);

        posts.setPage(dataJsonObj.getJSONObject("photos").getInt("page"));
        posts.setPagesCount(dataJsonObj.getJSONObject("photos").getInt("pages"));
        JSONArray array = dataJsonObj.getJSONObject("photos").getJSONArray("photo");
        for(int i=0; i<array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            Post post = new Post();
            post.setId        (object.getLong("id"));
            post.setUsername  (object.getString("ownername"));
            post.setTime      (object.getLong("dateupload"));
            post.setImageURL  (object.getString("url_m"));
            post.setTags(object.getString("tags"));
            posts.getPostsList().add(post);
        }
        log("FlikrService parseJSON returning list size = " + posts.getPostsList().size());
        return posts;
    }
}