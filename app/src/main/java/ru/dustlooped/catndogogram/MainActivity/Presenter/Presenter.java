package ru.dustlooped.catndogogram.MainActivity.Presenter;

import java.util.ArrayList;
import java.util.HashSet;

import ru.dustlooped.catndogogram.MainActivity.IView;

/**
 * Created by dustlooped on 31.05.2016.
 */
public interface Presenter {

    void onCreate(IView view, boolean afterOrientationChange);
    void onActivityFocusChange();
    void loadNewPosts(HashSet<String> tags, boolean isANDmode);
    void refreshLastResult();
    void loadNextPosts();
    void onDestroy();
}