package ru.dustlooped.catndogogram.MainActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.HashSet;

import ru.dustlooped.catndogogram.R;

/**
 * Created by dustlooped on 01.06.2016.
 */
public class TagInputDialog extends AppCompatDialogFragment {

    private EditText tagInputField1;
    private EditText tagInputField2;
    private EditText tagInputField3;
    private EditText tagInputField4;
    private EditText tagInputField5;
    private CheckBox tagInputCheckBox;

    public interface TagInputDialogListener {
        void onTagInputDialogResult(HashSet<String> tags, boolean isANDmode);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialog = getActivity().getLayoutInflater().inflate
                (R.layout.tag_input_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        tagInputField1 =   (EditText) dialog.findViewById(R.id.tag_input_field1);
        tagInputField2 =   (EditText) dialog.findViewById(R.id.tag_input_field2);
        tagInputField3 =   (EditText) dialog.findViewById(R.id.tag_input_field3);
        tagInputField4 =   (EditText) dialog.findViewById(R.id.tag_input_field4);
        tagInputField5 =   (EditText) dialog.findViewById(R.id.tag_input_field5);
        tagInputCheckBox = (CheckBox) dialog.findViewById(R.id.tag_input_checkBox);

        builder.setMessage("Search tags")
                .setView(dialog)
                .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int id) {
                        TagInputDialog.this.returnResult();
                    }
                })
                .setNegativeButton("Cancel", null);
        return builder.create();
    }

    private void returnResult() {
        HashSet<String> tags = new HashSet<>();

        String field1 = tagInputField1.getText().toString().trim();
        if(!field1.isEmpty()) tags.add(field1);
        String field2 = tagInputField2.getText().toString().trim();
        if(!field2.isEmpty()) tags.add(field2);
        String field3 = tagInputField3.getText().toString().trim();
        if(!field3.isEmpty()) tags.add(field3);
        String field4 = tagInputField4.getText().toString().trim();
        if(!field4.isEmpty()) tags.add(field4);
        String field5 = tagInputField5.getText().toString().trim();
        if(!field5.isEmpty()) tags.add(field5);

        if (!tags.isEmpty()) {
            TagInputDialogListener activity = (TagInputDialogListener) getActivity();
            activity.onTagInputDialogResult(tags, tagInputCheckBox.isChecked());
        }
    }
}