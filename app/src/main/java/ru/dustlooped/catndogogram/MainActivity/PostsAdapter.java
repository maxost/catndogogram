package ru.dustlooped.catndogogram.MainActivity;

import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.R;
import ru.dustlooped.catndogogram.Util.TimeConvert;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 31.05.2016.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {

    private List<Post> mPostsList = new ArrayList<>();

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        private TextView userName;
        private TextView time;
        private ImageView mainImage;
        private TextView tags;

        public PostViewHolder(View v) {
            super(v);
            userName =    (TextView)  v.findViewById(R.id.view_post_username);
            time =        (TextView)  v.findViewById(R.id.view_post_time);
            mainImage =   (ImageView) v.findViewById(R.id.view_post_mainimage);
            tags = (TextView)  v.findViewById(R.id.view_post_tags);
        }
    }

    void appendNewData(List posts) {
        mPostsList.addAll(posts);
        log("Adapter list size = " + mPostsList.size());
        notifyDataSetChanged();
    }

    void invalidateList() {
        mPostsList.clear();
    }

    void clearList() {
        mPostsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v  = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_post, parent, false);
        return new PostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        holder.userName.setText(mPostsList.get(position).getUsername());
        holder.time.setText(TimeConvert.getDateStringFromLong(mPostsList.get(position).getTime()));
        holder.tags.setText(mPostsList.get(position).getTags());
        Picasso
                .with(holder.mainImage.getContext())
                .load(mPostsList.get(position).getImageURL())
                .into(holder.mainImage);
    }

    @Override
    public int getItemCount() {
        return mPostsList.size();
    }

    @Override
    public long getItemId(int position) {
        return mPostsList.get(position).getId();
    }
}
