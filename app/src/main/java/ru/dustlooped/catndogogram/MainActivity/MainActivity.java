package ru.dustlooped.catndogogram.MainActivity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashSet;
import java.util.List;

import ru.dustlooped.catndogogram.MainActivity.Presenter.MainActivityPresenter;
import ru.dustlooped.catndogogram.MainActivity.Presenter.Presenter;
import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.R;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

public class MainActivity extends AppCompatActivity implements IView, TagInputDialog.TagInputDialogListener {

    private Presenter mMainPresenter = MainActivityPresenter.getInstance();

    private SwipeRefreshLayout  mSwipeRefreshLayout;
    private LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    private PostsAdapter        mPostsAdapter = new PostsAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("MainActivity onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initSwipeToRefreshLayout();
        initRecyclerView();
        mMainPresenter.onCreate(this, savedInstanceState!=null);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) { //because here we can start animation
        log("MainActivity onWindowFocusChanged");
        super.onWindowFocusChanged(hasFocus);
        mMainPresenter.onActivityFocusChange();
    }

    private void initSwipeToRefreshLayout() {
        log("MainActivity initSwipeToRefreshLayout");
        mSwipeRefreshLayout
                = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mMainPresenter.refreshLastResult();
            }
        });
    }

    private void initRecyclerView() {
        log("MainActivity initRecyclerView");
        mPostsAdapter.setHasStableIds(true);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mPostsAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                //if bottom of screen near second post from end
                if ( (mLayoutManager.getChildCount()
                        + mLayoutManager.findFirstVisibleItemPosition())
                        >= mLayoutManager.getItemCount()-2) {
                    mMainPresenter.loadNextPosts();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_options, menu);
        MenuItem resetScrollItem = menu.findItem(R.id.main_activity_reset_scroll);
        resetScrollItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log("MainActivity OnMenuItemClick main_activity_reset_scroll");
                resetScrollPosition();
                return false;
            }
        });
        MenuItem searchItem = menu.findItem(R.id.main_activity_search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                log("MainActivity OnMenuItemClick main_activity_search");
                new TagInputDialog().show(getSupportFragmentManager(), null);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onTagInputDialogResult(HashSet<String> tags, boolean isANDmode) {
        log("MainActivity onTagInputDialogResult");
        mMainPresenter.loadNewPosts(tags, isANDmode);
    }

    @Override
    public void updateList(List<Post> list) {
        log("MainActivity updateList; list size = " + list.size());
        mPostsAdapter.appendNewData(list);
    }

    @Override
    public void showEmptyView() {
        log("MainActivity showEmptyView");
        mPostsAdapter.clearList();
        Toast.makeText(this, "Matches not found", Toast.LENGTH_LONG).show();
    }

    @Override
    public void invalidateList() {
        log("MainActivity invalidateList");
        mPostsAdapter.invalidateList();
    }

    @Override
    public void resetScrollPosition() {
        log("MainActivity resetScrollPosition");
        mLayoutManager.scrollToPosition(0);
    }


    @Override
    public void showLoadAnimation(boolean show) {
        log("MainActivity showLoadAnimation = " + show);
        mSwipeRefreshLayout.setRefreshing(show);
        if(!show) {
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void showError(String message) {
        log("MainActivity showError = " + message);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        log("MainActivity onDestroy");
        mMainPresenter.onDestroy();
        super.onDestroy();
    }
}