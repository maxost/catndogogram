package ru.dustlooped.catndogogram.MainActivity.Presenter;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import ru.dustlooped.catndogogram.Cache.Cache;
import ru.dustlooped.catndogogram.Cache.PreferencesCache;
import ru.dustlooped.catndogogram.MainActivity.IView;
import ru.dustlooped.catndogogram.Model.Post;
import ru.dustlooped.catndogogram.Model.Posts;
import ru.dustlooped.catndogogram.Networking.FlikrService;
import ru.dustlooped.catndogogram.Networking.HttpClient.BuiltInHttpClient;
import ru.dustlooped.catndogogram.Networking.Networking;

import static ru.dustlooped.catndogogram.Util.MyLog.log;

/**
 * Created by dustlooped on 31.05.2016.
 */
public class MainActivityPresenter implements Presenter {

    private static MainActivityPresenter instance = new MainActivityPresenter();

    private WeakReference<IView> mView;
    private Networking mNetworking = new FlikrService(new BuiltInHttpClient());
    private Cache mCache = PreferencesCache.getInstance();

    private HashSet<String> currentTags = new HashSet<>(Arrays.asList("cat", "dog"));
    private int             currentPage;
    private int             pagesCount;
    private boolean         isANDmode;

    private AsyncTask mFetchPostsTask;

    private boolean appStarting = true;

    public static MainActivityPresenter getInstance() {
        return instance;
    }

    @Override
    public void onCreate(IView view, boolean afterOrientationChange) {
        log("MainActivityPresenter onCreate, afterOrientationChange = " + afterOrientationChange);
        mView = new WeakReference<>(view);

        //show posts from cache
        ArrayList<Post> cachedPostsList = mCache.getPostsList();
        if(cachedPostsList!=null) mView.get().updateList(cachedPostsList);

        if(!afterOrientationChange && appStarting) {
            mCache.clearCache();
            fetchPosts();
        }
    }

    @Override
    public void onActivityFocusChange() {
        log("MainActivityPresenter onActivityFocusChange");
        if(appStarting) {
            appStarting = false;
            mView.get().showLoadAnimation(true);
        }
    }

    @Override
    public void loadNewPosts(HashSet<String> tags, boolean isANDmode) {
        log("MainActivityPresenter getPostsList");
        mCache.clearCache();
        currentTags = tags;
        currentPage = 1;
        this.isANDmode = isANDmode;
        fetchPosts();
        mView.get().showLoadAnimation(true);
    }

    @Override
    public void refreshLastResult() {
        log("MainActivityPresenter refreshLastResult");
        mCache.clearCache();
        currentPage = 1;
        fetchPosts();
        mView.get().showLoadAnimation(true);
    }

    @Override
    public void loadNextPosts() {
        if(mFetchPostsTask.getStatus()!=AsyncTask.Status.RUNNING && currentPage<pagesCount) {
            log("MainActivityPresenter loadNextPosts");
            currentPage++;
            fetchPosts();
        }
    }

    private void fetchPosts() {
        log("MainActivityPresenter fetchPosts");
        if (mFetchPostsTask!=null) mFetchPostsTask.cancel(true);
        mFetchPostsTask = new AsyncTask<Void, Void, Object>() {
            @Override
            protected Object doInBackground(Void... params) {
                Posts posts = null;
                try {
                    posts = mNetworking.getPosts(currentTags, currentPage, isANDmode);
                } catch (Exception e) {
                    return e;
                }
                return posts;
            }

            @Override
            protected void onPostExecute(Object result) {
                log("MainActivityPresenter mFetchPostsTask onPostExecute");
                if (result instanceof Exception) {
                    handleFetchError((Exception) result);
                    return;
                }
                handleFetchResult((Posts) result);
            }
        }.execute();
    }

    private void handleFetchResult(Posts posts) {
        log("MainActivityPresenter handleFetchResult");
        mCache.putPosts(posts);
        currentPage = posts.getPage(); //in case of fire
        pagesCount = posts.getPagesCount();
        if(posts.getPostsList().isEmpty() && currentPage==1) {
            mView.get().showEmptyView();
        } else {
            if(currentPage==1) {
                mView.get().invalidateList();
                mView.get().resetScrollPosition();
            }
            mView.get().updateList(posts.getPostsList());
        }
        mView.get().showLoadAnimation(false);
    }

    private void handleFetchError(Exception e) {
        log("MainActivityPresenter handleFetchError");
        e.printStackTrace();
        if (e instanceof IOException) {
            mView.get().showError("Network error");
        } else if(e instanceof JSONException) {
            mView.get().showError("JSONException");
        } else if (e instanceof UnsupportedEncodingException) {
            mView.get().showError("UnsupportedEncodingException");
        } else if (e instanceof MalformedURLException) {
            mView.get().showError("MalformedURLException");
        } else {
            mView.get().showError("Uknown Error");
        }
        mView.get().showLoadAnimation(false);
    }

    @Override
    public void onDestroy() {
        log("MainActivityPresenter onDestroy");
    }
}