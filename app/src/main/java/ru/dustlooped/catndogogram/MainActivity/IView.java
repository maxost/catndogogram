package ru.dustlooped.catndogogram.MainActivity;

import java.util.List;

import ru.dustlooped.catndogogram.Model.Post;

/**
 * Created by dustlooped on 31.05.2016.
 */
public interface IView {
    void updateList(List<Post> list);
    void showEmptyView();
    void showLoadAnimation(boolean show);
    void invalidateList();
    void resetScrollPosition();
    void showError(String message);
}