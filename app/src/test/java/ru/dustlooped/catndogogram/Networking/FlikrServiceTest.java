package ru.dustlooped.catndogogram.Networking;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import ru.dustlooped.catndogogram.Model.Posts;
import ru.dustlooped.catndogogram.Networking.HttpClient.BuiltInHttpClient;
import ru.dustlooped.catndogogram.Networking.HttpClient.StubHttpClient;
import ru.dustlooped.catndogogram.Util.MyLog;

import static org.junit.Assert.*;

/**
 * Created by dustlooped on 04.06.2016.
 */
public class FlikrServiceTest {

    @Test
    public void testGetPostsFromMock() throws Exception {
        MyLog.setEnabled(false);
        Networking networking = new FlikrService(new StubHttpClient());
        Posts posts = networking.getPosts(new HashSet<>(Arrays.asList("cat", "dog")), 1, false);

        assertEquals(posts.getPagesCount(), 27801);
        assertEquals(posts.getPage(), 1);
        assertEquals(posts.getPostsList().size(), 10);
        assertEquals(posts.getPostsList().get(0).getTime(), 1465018515);
    }

    @Test
    public void testGetPostsFromNet() throws Exception {
        MyLog.setEnabled(false);
        Networking networking = new FlikrService(new BuiltInHttpClient());
        Posts posts = networking.getPosts(new HashSet<>(Arrays.asList("cat", "dog")), 1, false);

        assertEquals(posts.getPage(), 1);
        assertEquals(posts.getPostsList().size(), 10);
        assertNotNull(posts.getPostsList().get(0).getUsername());
    }
}